[![Build Status](https://semaphoreci.com/api/v1/dekken/mkn-ram/branches/master/badge.svg)](https://semaphoreci.com/dekken/mkn-ram)

# mkn.ram

**C++ Remote Applications Management** 

[README](https://raw.githubusercontent.com/mkn/mkn.ram/master/README.noformat)

## Dependencies 

[mkn.kul](https://github.com/mkn/mkn.kul)
